{
    function iconActivator(event) {
        if (event.target.closest('img')) {
            event.target.style.opacity = 1;
        }
        for (let ico of iconList.children) {
            if (ico !== event.target.closest('img')) {
                ico.removeAttribute('style');
            }
        }
    }

    iconList.addEventListener("click", iconActivator);
}