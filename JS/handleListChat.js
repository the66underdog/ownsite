{
    const mediaQuery620 = window.matchMedia('(max-width: 620px)');

    function animateListChannels() {
        if (listChat.style.display === 'none') {
            listChat.removeEventListener('animationend', popUpInvListChat);
            listChat.addEventListener('animationstart', popUpListChat);
            showListChannels();
        }
        else {
            listChat.addEventListener('animationend', popUpInvListChat);
            hideListChannels();
        }
    }

    function handleMediaQuery(event) {
        if (event.matches) {
            if (listChat.style.display !== 'none') {
                listChat.addEventListener('animationend', popUpInvListChat);
                hideListChannels();
            }
        }
        else if (listChat.style.display === 'none') {
            listChat.removeEventListener('animationend', popUpInvListChat);
            listChat.addEventListener('animationstart', popUpListChat);
            showListChannels();
        }
    }

    function showListChannels() {
        listChat.style.cssText = `animation: pop-up-list-chat 1s
        `;
        listChatBtn.style.cssText = `left: calc(12rem + 12.571428vw);
            transition-duration: 1s; 
        `;
        headerChat.style.cssText += `
            grid-column-start: chat-list;
        `;
        chat.style.cssText += `
            grid-column-start: chat-list;
        `;
        chatForm.style.cssText += `
            grid-column-start: chat-list;
        `;
        listChatImg.style.cssText = `
            transform: rotate(90deg) translate(0%, 132%);
            transition-duration: 0.66s;
        `;
    }

    function hideListChannels() {
        listChat.style.cssText = `animation: pop-up-inv-list-chat 1s
        `;
        listChatBtn.style.cssText = `left: calc(2rem + 3.285714vw);
            transition-duration: 1s;
        `;
        headerChat.style.cssText += `
            grid-column-start: left-icons;
        `;
        chat.style.cssText += `
            grid-column-start: left-icons;
        `;
        chatForm.style.cssText += `
            grid-column-start: left-icons;
        `;
        listChatImg.style.cssText = `
            transform: rotate(-90deg) translate(0%, -132%);
            transition-duration: 0.66s;
        `;
    }

    function popUpListChat() {
        listChat.style.display = 'flex';
    }

    function popUpInvListChat() {
        listChat.style.display = 'none';
    }

    listChatBtn.addEventListener('click', animateListChannels);
    mediaQuery620.addEventListener('change', handleMediaQuery);
    handleMediaQuery(mediaQuery620);
}