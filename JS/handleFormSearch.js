{
    const mediaQuery840 = window.matchMedia('(max-width: 840px)');

    function handleSearch(event) {
        event.preventDefault();
    }

    function handleMediaQuery(event) {
        if (event.matches) {
            formSearch.removeEventListener('transitionstart', rollSearchInput);
            formSearchInp.style.display = 'none';
            formSearch.style.cssText = `
                width: 0px;
                transition-duration: 1s;
            `;
            formSearch.lastElementChild.style.cssText = `
                margin-left: -11px;
                transition-duration: 1s;
            `;
        }
        else {
            formSearch.addEventListener('transitionstart', rollSearchInput);
            formSearch.removeAttribute('style');
            formSearch.style.cssText = `
                width: calc(5rem + 12.291667vw);
                transition-duration: 1s;
            `;
            formSearch.lastElementChild.style.cssText = `
                margin-left: -22px;
            `;
        }
    }

    function rollSearchInput() {
        formSearchInp.style.display = 'inline-block';
    }

    formSearch.addEventListener('submit', handleSearch)
    mediaQuery840.addEventListener("change", handleMediaQuery);
    handleMediaQuery(mediaQuery840);
}