const listObj = {};

function channelActivator(event) {
    let channelName = '';
    if (event.target.closest('li')) {
        event.target.closest('li').style.cssText = `
            color: rgba(255, 255, 255, 1);
            background: rgba(255, 255, 255, 0.2);
            border-radius: 4px;
        `;
        event.target.closest('li').setAttribute('id', 'chn-active');
        addChannelInfo(event.target.closest('li').innerHTML);
        for (let i = 1; i < channelHeader.innerHTML.length; i++) {
            channelName += channelHeader.innerHTML[i];
        }
    }
    userCount.parentElement.style.cssText = `
        display: flex;
        flex-flow: row nowrap;
        align-items: center;
    `;
    userCount.parentElement.previousElementSibling.style.display = "inline"
    typeForm.parentElement.parentElement.style.display = "flex";

    userCount.innerHTML = Math.floor(Math.random() * 1001);
    for (let channel of channelsList.children) {
        if (channel.tagName == 'LI') {
            if (channel !== event.target) {
                channel.removeAttribute('style');
                channel.removeAttribute('id');
            }
        }
    }
    chatViewport.innerHTML = '';
    let chatsContainer = JSON.parse(sessionStorage.getItem('chatsContainer'));
    if (channelName in chatsContainer) {
        if (!(chatsContainer[channelName].messages.length == 0)) {
            for (let y = 0; y < chatsContainer[channelName].messages.length; y++) {
                chatViewport.append(createMessage(chatsContainer[channelName].messages[y]));
            }

        }
        else {
            let note = document.createElement('span');
            note.innerHTML = '# This chat is empty for now, feel free to type a message!';
            note.style.cssText = `
                    color: rgba(141, 141, 141, 0.5);
                    font-size: calc(0.5rem + 0.96428vw);
                    font-weight: 400;
                    position: absolute;
                    width: 100%;
                    bottom: 0.5%;
                    left: 0px;
                    text-align: center;
                `;
            chatViewport.append(note);
        }
    }
    else {
        registerChannel(channelName);
    }
}

function addChannelInfo(chlName) {
    let channelNameBuffer = '';
    for (let symb of chlName) {
        if (!(symb == " ")) {
            channelNameBuffer += symb;
        }
    }
    channelHeader.innerHTML = channelNameBuffer;
    typeForm.setAttribute('placeholder', `Message in ${channelNameBuffer}`);
}

function registerChannel(chlName) {
    listObj[chlName] = {
        messages: [],
        fromTop: 0,
    };
}

function regAllChannels() {
    if (sessionStorage.getItem('chatsContainer') === null) {
        let channelName;
        for (let channel of channelsList.children) {
            channelName = '';
            for (let i = 2; i < channel.innerHTML.length; i++) {
                channelName += channel.innerHTML[i];
            }
            registerChannel(channelName);
        }
        sessionStorage.setItem('chatsContainer', JSON.stringify(listObj));
    }
}

function createMessage(message) {
    if (chatViewport.firstChild) {
        if (chatViewport.firstChild.tagName === 'SPAN')
            chatViewport.innerHTML = '';
    }
    let txtBuf = '';
    let messageBlock = document.createElement('article');
    messageBlock.style.cssText = `
                margin: 6px 10px;
                max-width: 97.7%;
            `;
    let ava = document.createElement('div');
    for (let y = 1; y < message.ava.length; y++) {
        txtBuf += message.ava[y];
    }
    ava.style.cssText = `
                display: inline-block;
                margin-right: calc(0.5rem + 0.24305vw);
                width: 32px;
                height: 32px;
                max-width: 32px;
                max-height: 32px;
                background-image: url("./Images/${txtBuf}.png");
                background-repeat: no-repeat;
                background-size: cover;
                border-radius: 4px;
                vertical-align: top;
            `;
    let contentBlock = document.createElement('div');
    contentBlock.style.cssText = `
                display: inline-block;
                white-space: normal;
                max-width: 90%;
            `;
    let author = document.createElement('h3');
    author.innerHTML = message.author;
    author.style.cssText = `
                font-size: 12px;
                font-weight: 700;
                color: black;
                line-height: 14.4px;
                margin: 0px;
            `;
    let timeDate = document.createElement('span');
    timeDate.innerHTML = message.time;
    timeDate.style.cssText = `
                font-size: 12px;
                color: rgba(141, 141, 141, 1);
                line-height: 14.4px;
                font-weight: 400;
            `;
    let content = document.createElement('span');
    content.innerHTML = message.content;
    content.style.cssText = `
                display: inline-block;
                font-size: 13px;
                color: black;
                line-height: 20px;
                word-wrap: break-word;
                white-space: pre-wrap;
                background-color: rgba(223, 223, 223, 0.5);
                border-radius: 6px;
                box-sizing: border-box;
                padding: 2px 8px 4px;
                max-width: 100%;
            `;
    author.append(timeDate);
    contentBlock.append(author);
    contentBlock.append(content)
    messageBlock.append(ava);
    messageBlock.append(contentBlock)
    return messageBlock;
}

regAllChannels();
channelsList.addEventListener("click", channelActivator);
