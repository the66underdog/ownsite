{
    function chatMessage(event) {
        if (event.key === 'Enter' && !event.shiftKey) {
            event.preventDefault();
            if (typeForm.value !== "" && typeForm.value.match(/\S/g)) {
                let messageObj = {};
                for (let channel of channelsList.children) {
                    if (channel.id == 'chn-active') {
                        let channelName = '';
                        for (let i = 2; i < channel.innerHTML.length; i++) {
                            channelName += channel.innerHTML[i];
                        }
                        let date = new Date();
                        let minutes = date.getMinutes() < 10 ? `0${date.getMinutes()}` : date.getMinutes();
                        let apM = date.getHours() < 12 ? 'AM' : 'PM';
                        messageObj.ava = `n000${Math.round(Math.random() * (numValue[1].innerHTML - 1))}`;
                        messageObj.author = document.querySelector(`[data-num="${messageObj.ava}"]`).nextElementSibling.innerHTML;
                        messageObj.time = date.getHours() + ':' + minutes + ` ${apM}`
                        messageObj.content = typeForm.value;
                        typeForm.value = '';
                        let chatsContainer = JSON.parse(sessionStorage.getItem('chatsContainer'));
                        chatsContainer[channelName].messages.push(messageObj);
                        sessionStorage.removeItem('chatsContainer');
                        sessionStorage.setItem('chatsContainer', JSON.stringify(chatsContainer));
                        chatViewport.append(createMessage(messageObj));
                        chatViewport.lastElementChild.scrollIntoView({ behavior: "smooth" });
                    }
                }
            }
        }
    }

    chatForm.addEventListener("keypress", chatMessage);
}
