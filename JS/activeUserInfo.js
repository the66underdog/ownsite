{
    const userInfo = document.querySelectorAll(`.user-cv, .info-username, .info-email, 
.info-contacts, .info-timezone`);
    const userObj = {};

    function registerUser(udn, usr) {
        let txtBuf = '';
        for (let i = 1; i < udn.toString().length; i++) {
            txtBuf += udn[i];
        }
        if (!(udn in userObj)) {
            let userObjInto = {};
            userObjInto["Full name"] = usr.lastElementChild.innerHTML.replace(/\s/g, '');

            userObjInto.avatar = `Images/${txtBuf}.png`;
            userObjInto.email = usr.lastElementChild.dataset.email;
            userObjInto.timezoneDif = (Math.random() < 0.5) ? -Math.round(Math.random() * 4) : Math.round(Math.random() * 4);

            if (usr.lastElementChild.hasAttribute('data-usname')) {
                userObjInto.usname = usr.lastElementChild.dataset.usname;
            }
            if (usr.lastElementChild.hasAttribute('data-skype')) {
                userObjInto.skype = usr.lastElementChild.dataset.skype;
            }
            userObj[udn] = userObjInto;
        }
    }

    function activeUserInfo(event) {
        for (let user of users.children) {
            if (user.id == 'user-active') {
                let userDataNum = user.firstElementChild.nextElementSibling.dataset.num;
                if (!(userDataNum in userObj)) {
                    registerUser(userDataNum, user);
                }
                let date = new Date();
                userAva[0].setAttribute('src', `${userObj[userDataNum].avatar}`);
                userInfo[0].innerHTML = userObj[userDataNum]["Full name"];
                userInfo[1].lastElementChild.innerHTML = userObj[userDataNum].usname;
                userInfo[2].lastElementChild.innerHTML = userObj[userDataNum].email;
                userInfo[3].lastElementChild.innerHTML = userObj[userDataNum].skype;
                let hours = date.getHours() + userObj[userDataNum].timezoneDif;
                if (hours >= 24) hours -= 24; 
                if (hours < 0) hours += 24; 
                let apM = hours < 12 ? 'AM' : 'PM';
                let minutes = date.getMinutes() < 10 ? `0${date.getMinutes()}` : date.getMinutes();
                userInfo[4].lastElementChild.innerHTML = hours + ':' + minutes + ` ${apM} Local time`;
            }
        }
    }

    users.addEventListener("click", activeUserInfo);
    window.onload = function () {
        for (let user of users.children) {
            registerUser(user.firstElementChild.nextElementSibling.dataset.num, user);
        }
    }
}