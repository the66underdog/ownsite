{
    let actUser;

    function activeUser(event) {
        switch (event.type) {
            case "click":
                if (event.target.closest('li')) {
                    actUser = event.target.closest('li');
                    event.target.closest('li').setAttribute('id', 'user-active');
                    event.target.closest('li').style.cssText = `
                    background-color: rgba(255, 255, 255, 0.25);
                    border-radius: 8px;
                `;
                    if (userInfo.style.display === 'none') {
                        userInfo.removeEventListener('animationend', popUpInvUserInfo);
                        userInfo.addEventListener('animationstart', popUpUserInfo);
                        userInfo.style.cssText = `animation: pop-up 1s
                `;
                        headerChat.style.cssText = `
                    grid-column-start: chat-list;
                    grid-column-end: v-chat;
                `;
                        chat.style.cssText = `
                    grid-column-start: chat-list;
                    grid-column-end: v-chat;
                `;
                        chatForm.style.cssText = `
                    grid-column-start: chat-list;
                    grid-column-end: v-chat;
                `;
                        userInfoArrow.style.cssText = `
                    transform: rotate(-90deg);
                    transition-duration: 0.66s;
                `;
                    }
                }
                for (let user of users.children) {
                    if (user !== event.target.closest('li')) {
                        user.removeAttribute('id');
                        user.style.cssText = `
                        background-color: transparent;
                    `;
                    }
                }
                break;
            case "mousedown":
                if (event.target.closest('li')) {
                    event.target.closest('li').style.cssText = `
                        background-color: rgba(255, 255, 255, 0.5);
                        border-radius: 8px;
                    `;
                }
                break;
            case "mouseover":
                if (event.target.closest('li')) {
                    event.target.closest('li').style.cssText = `
                    background-color: rgba(255, 255, 255, 0.15);
                    border-radius: 8px;
                `;
                }
                break;
            case "mouseout":
                if (event.target.closest('li')) {
                    if (event.target.closest('li') !== actUser) {
                        event.target.closest('li').style.cssText = `
                    background-color: transparent;
                `;
                    }
                }
                break;
        }
    }

    /*Very expensive doing CSS effects for the cost of four listeners, 
    has been made for practice purposes */
    users.addEventListener("click", activeUser);
    users.addEventListener("mousedown", activeUser);
    users.addEventListener("mouseover", activeUser);
    users.addEventListener("mouseout", activeUser);
}