{
    const matchedMessagesBlock = document.querySelector('.matched-messages');
    const mediaQuery660 = window.matchMedia('(max-width: 660px)');

    function animateUserInfo() {
        if (userInfo.style.display === 'none') {
            userInfo.removeEventListener('animationend', popUpInvUserInfo);
            userInfo.addEventListener('animationstart', popUpUserInfo);
            showUserInfo();
        }
        else {
            userInfo.addEventListener('animationend', popUpInvUserInfo);
            hideUserInfo();
        }
    }

    function handleMediaQuery(event) {
        if (event.matches) {
            if (userInfo.style.display !== 'none') {
                userInfo.addEventListener('animationend', popUpInvUserInfo);
                hideUserInfo();
            }
        }
        else if (userInfo.style.display === 'none') {
            userInfo.removeEventListener('animationend', popUpInvUserInfo);
            userInfo.addEventListener('animationstart', popUpUserInfo);
            showUserInfo();
        }
    }

    function showUserInfo() {
        userInfo.style.cssText = `animation: pop-up 1s
        `;
        headerChat.style.cssText += `
            grid-column-end: v-chat;
        `;
        chat.style.cssText += `
            grid-column-end: v-chat;
        `;
        chatForm.style.cssText += `
            grid-column-end: v-chat;
        `;
        userInfoArrow.style.cssText = `
            transform: rotate(-90deg);
            transition-duration: 0.66s;
        `;
        matchedMessagesBlock.style.right = 'calc(7rem + 7vw)';
    }

    function hideUserInfo() {
        userInfo.style.cssText = `animation: pop-up-inv 1s
        `;
        headerChat.style.cssText += `
            grid-column-end: user-info;
        `;
        chat.style.cssText += `
            grid-column-end: user-info;
        `;
        chatForm.style.cssText += `
            grid-column-end: user-info;
        `;
        userInfoArrow.style.cssText = `
            transform: rotate(90deg);
            bottom: calc(4rem + 1.94444vw);
            filter: none;
            background-color: rgba(0, 0, 0, 0.5);
            border-radius: 4px;
            transition-duration: 0.66s;
        `;
        matchedMessagesBlock.style.right = '0px';
    }

    function popUpUserInfo() {
        userInfo.style.display = 'block';
    }

    function popUpInvUserInfo() {
        userInfo.style.display = 'none';
    }

    userInfoArrow.addEventListener('click', animateUserInfo);
    mediaQuery660.addListener(handleMediaQuery);

    handleMediaQuery(mediaQuery660);
}