{
    const mediaQuery840 = window.matchMedia('(max-width: 840px)');

    function handleMediaQuery(event) {
        if (event.matches) {
            searchBtn.removeEventListener('click', findMsg);
            formSearchInp.removeEventListener('focus', findMsg);
            formSearchInp.removeEventListener('input', findMsg);
            formSearchInp.removeEventListener('focusout', hideMatchedMessage);

            searchBtn.addEventListener('click', searchInputPopUp);
            formSearchInp840.addEventListener('focus', findMsg);
            formSearchInp840.addEventListener('input', findMsg);
            formSearchInp840.addEventListener('focusout', hideMatchedMessage);
        }
        else {
            formSearchInp840.style.display = 'none';
            formSearchInp840.removeEventListener('input', findMsg);
            searchBtn.removeEventListener('click', searchInputPopUp);
            formSearchInp.addEventListener('focus', findMsg);
            formSearchInp.addEventListener('input', findMsg);
            formSearchInp.addEventListener('focusout', hideMatchedMessage);
        }
    }

    function findMsg(event) {
        if (event.target.value.length === 0) {
            matchedMessagesBlock.style.display = 'none';
            showMatchedMessage();
        }
        else {
            let activeChannel = '';
            let arrBuf = [];
            for (let channel of channelsList.children) {
                if (channel.id == 'chn-active') {
                    for (let i = 2; i < channel.innerHTML.length; i++) {
                        activeChannel += channel.innerHTML[i];
                    }
                    break;
                }
            }
            let chatsContainer = JSON.parse(sessionStorage.getItem('chatsContainer'));
            for (let message of chatsContainer[activeChannel].messages) {
                const regex = new RegExp(`${event.target.value}`, 'mi');
                if (message.content.match(regex) !== null) {
                    arrBuf.push(message);
                }
            }
            showMatchedMessage(arrBuf);
        }
    }

    function searchInputPopUp() {
        if (formSearchInp840.style.display === 'none') {
            formSearchInp840.style.cssText = `
            display: inline-block;
            position: absolute;
        `;
            formSearchInp840.style.left = `${-formSearchInp840.offsetWidth}px`;
        }
        else {
            formSearchInp840.style.display = 'none';
        }
    }

    function hideMatchedMessage() {
        matchedMessagesBlock.style.display = 'none';
    }

    function showMatchedMessage(arr = []) {
        if (matchedMessagesBlock.style.display === 'none') {
            matchedMessagesBlock.style.display = 'flex';
        }
        else {
            matchedMessagesBlock.innerHTML = '';
        }
        if (arr.length === 0) {
            let note = document.createElement('span');
            note.innerHTML = `# No messages has been found.
            Try another key string.   
            `;
            note.style.cssText = `
                    color: rgba(141, 141, 141, 0.5);
                    font-size: calc(0.5rem + 0.96428vw);
                    font-weight: 400;
                    margin: 10% auto;
                    white-space: pre;
                `;
            matchedMessagesBlock.innerHTML = '';
            matchedMessagesBlock.append(note);
        }
        else {
            for (let msg of arr) {
                matchedMessagesBlock.append(createMessage(msg));
            }
        }
    }

    mediaQuery840.addEventListener('change', handleMediaQuery);
    handleMediaQuery(mediaQuery840);
}