{
    function addAva() {
        for (const friendElem of users.children) {
            let i = 0;
            for (const avaBlk of friendElem.children) {
                if (avaBlk.hasAttribute('data-num')) {
                    var textBuf = '';
                    avaBlkStr = avaBlk.dataset.num.toString();
                    for (let y = 1; y < avaBlkStr.length; y++) {
                        textBuf += avaBlk.dataset.num[y];
                    }
                    avaBlk.style.cssText = `
                width: 32px;
                max-width: 32px;
                height: 32px;
                max-height: 32px;
                background-image: url("./Images/${textBuf}.png");
                background-repeat: no-repeat;
                background-size: cover;
                border-radius: 4px;
                `;
                    i++;
                    textBuf = '';
                }
            }
        }
    }

addAva();
}